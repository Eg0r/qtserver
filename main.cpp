#include <QCoreApplication>

#include "server.h"
#include "directoryscanner.h"
#include "cacheanalyzator.h"
#include "diskanalyzator.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    DirectoryScanner* directoryScanner = new DirectoryScanner();
    directoryScanner->walk(Server::ROOT_DIRECTORY);

    Server* server = new Server();
    server->start();


    return a.exec();
}
