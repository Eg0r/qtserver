#include "cacheanalyzator.h"

CacheAnalyzator::CacheAnalyzator()
{

}

QString CacheAnalyzator::createPeriod(uint dateTime1, uint dateTime2)
{
    QString queryString = QString("//file[@create_date>=" + QString::number(dateTime1) + " and @create_date<=" + QString::number(dateTime2) + "]");
    QDomNodeList result = xPathQueryRun(queryString);

    QDomDocument* document = new QDomDocument("period");
    QDomProcessingInstruction xmlProcessingInstruction = document->createProcessingInstruction("xml", "version=\"1.0\"");
    document->appendChild(xmlProcessingInstruction);
    QDomElement element = document->createElement("data");
    document->appendChild(element);

    for (int i = 0; i < result.count(); i++)
    {
        QDomNode tmpNode = result.at(i).cloneNode();
        element.appendChild(tmpNode.toElement());
    }

    return document->toString();
}

QString CacheAnalyzator::createOld()
{
    QString queryString = "//file";
    QDomNodeList result = xPathQueryRun(queryString);
    int minCreateDate = result.at(0).toElement().attribute("create_date").toInt();
    int num = 0;
    for (int i = 0; i < result.count(); i++)
    {
        QDomNode tmpNode = result.at(i).cloneNode();
        int tmp = tmpNode.toElement().attribute("create_date").toInt();
        if (tmp < minCreateDate)
        {
            minCreateDate = tmp;
            num = i;
        }
    }

    QDomDocument* document = new QDomDocument("old");
    QDomProcessingInstruction xmlProcessingInstruction = document->createProcessingInstruction("xml", "version=\"1.0\"");
    document->appendChild(xmlProcessingInstruction);
    QDomElement element = document->createElement("data");
    document->appendChild(element);
    element.appendChild(result.at(num).toElement());
    return document->toString();
}

QString CacheAnalyzator::createNew()
{
    QString queryString = "//file";
    QDomNodeList result = xPathQueryRun(queryString);
    int maxCreateDate = result.at(0).toElement().attribute("create_date").toInt();
    int num = 0;
    for (int i = 0; i < result.count(); i++)
    {
        QDomNode tmpNode = result.at(i).cloneNode();
        int tmp = tmpNode.toElement().attribute("create_date").toInt();
        if (tmp > maxCreateDate)
        {
            maxCreateDate = tmp;
            num = i;
        }
    }

    QDomDocument* document = new QDomDocument("new");
    QDomProcessingInstruction xmlProcessingInstruction = document->createProcessingInstruction("xml", "version=\"1.0\"");
    document->appendChild(xmlProcessingInstruction);
    QDomElement element = document->createElement("data");
    document->appendChild(element);
    element.appendChild(result.at(num).toElement());
    return document->toString();
}

QDomNodeList CacheAnalyzator::xPathQueryRun(QString queryString)
{
    QXmlQuery* query = new QXmlQuery();

    QDomNodeList children;

    qDebug() << "start search period";

    QFile file("cache.xml");
    QString xmlString;
    if(file.open(QIODevice::ReadOnly))
    {
        xmlString = file.readAll();
        file.close();
    }
    else
    {
        throw new CacheFileNotFoundException();
    }

    if (query->setFocus(xmlString))
    {
        qDebug() << queryString;
        query->setQuery(queryString);
        if (query->isValid())
        {
            QString result;
            query->evaluateTo(&result);
            QDomDocument dom;
            dom.setContent(QString("<Data>%1</Data>").arg(result));
            children = dom.documentElement().childNodes();
        }
    }

    qDebug() << "search is end";

    return children;
}
