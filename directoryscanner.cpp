#include "directoryscanner.h"

DirectoryScanner::DirectoryScanner(QObject* parent) : QObject(parent)
{
    thread = new QThread();
    this->moveToThread(thread);
}

void DirectoryScanner::walk(QString rootPath)
{
    this->rootPath = rootPath;
    connect(thread, SIGNAL(started()), this, SLOT(threadWalk()));
    thread->start();
}

void DirectoryScanner::threadWalk()
{
    QDir* dir = new QDir(rootPath);
    qDebug() << "start scan";
    QDomDocument* document = new QDomDocument("cache");
    QDomProcessingInstruction xmlProcessingInstruction = document->createProcessingInstruction("xml", "version=\"1.0\"");
    document->appendChild(xmlProcessingInstruction);
    QDomElement element = document->createElement("root_directory");
    document->appendChild(element);
    recursiveWalk(dir, document, &element);

    QFile file("cache.xml");
    if(file.open(QIODevice::WriteOnly))
    {
        QTextStream(&file) << document->toString();
        file.close();
    }
    emit finishedScan();
    qDebug() << "end scan";
}


void DirectoryScanner::recursiveWalk(QDir* directory, QDomDocument* document, QDomElement* currentDirectory)
{
    QFileInfoList list = directory->entryInfoList();
    for (int i = 0; i < list.size(); i++)
    {
        QFileInfo fileInfo = list.at(i);

        if ((fileInfo.fileName() == ".") || (fileInfo.fileName() == ".."))
        {
            continue;
        }

        QDomElement domElement;
        if (fileInfo.isDir())
        {
            qDebug() << fileInfo.fileName();
            QDir* directoryDown = new QDir(list[i].absoluteFilePath());
            domElement = document->createElement("directory");
            createInfo(&domElement, &fileInfo);
            recursiveWalk(directoryDown, document, &domElement);
        }
        else
        {
            qDebug() << fileInfo.fileName();
            domElement = document->createElement("file");
            createInfo(&domElement, &fileInfo);
        }
        currentDirectory->appendChild(domElement);
    }
}

void DirectoryScanner::createInfo(QDomElement* element, QFileInfo* fileInfo)
{
    element->setAttribute("name", fileInfo->fileName());
    if (!fileInfo->isDir())
    {
        element->setAttribute("size_in_bytes", fileInfo->size());
    }
    element->setAttribute("create_date", fileInfo->created().toTime_t());
}



