#include "server.h"

const QString Server::ROOT_DIRECTORY = "./www";

Server::Server()
{
    cacheAnalyzator = new CacheAnalyzator();
    diskAnalyzator = new DiskAnalyzator();
}

void Server::start()
{
    if (listen(QHostAddress::Any, 80))
    {
        cout << "server started on 80 port" << endl;
    }
}

void Server::incomingConnection(int handle)
{
    QTcpSocket* socket = new QTcpSocket();
    socket->setSocketDescriptor(handle);
    cout << "incomming connection" << endl;
    connect(socket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
}

RequestType* Server::getRequestType(QByteArray data)
{
    data = data.remove(0, 4);
    int indexHttp = data.indexOf("HTTP") - 1;
    data = data.remove(indexHttp, data.length());
    int indexParams = data.indexOf("?");
    QString args = "";

    RequestType* requestType = new RequestType();

    if (indexParams >= 0)
    {
        args = data;
        data = data.remove(indexParams, data.length());
        args = args.remove(0, indexParams).remove(0, 1);
        requestType->setParams(args);
    }

    qDebug() << data << " " << args;

    if (data.indexOf("/cache") == 0)
    {
        requestType->type = RequestType::CACHE_REQUEST;
    }
    else if (data.indexOf("/old") == 0)
    {
        requestType->type = RequestType::OLD_REQUEST;
    }
    else if (data.indexOf("/new") == 0)
    {
        requestType->type = RequestType::NEW_REQUEST;
    }
    else if (data.indexOf("/period") == 0)
    {
        requestType->type = RequestType::PERIOD_REQUEST;
    }
    else if (data.indexOf("/disk_space") == 0)
    {
        requestType->type = RequestType::DISK_SPACE_REQUEST;
    }
    else if (data.indexOf("/download") == 0)
    {
        requestType->type = RequestType::DOWNLOAD_REQUEST;
    }
    else
    {
        requestType->type = RequestType::E404_REQUEST;
    }

    return requestType;
}

void Server::sendCache(QTcpSocket* socket)
{
    QFile file("cache.xml");
    if (file.open(QIODevice::ReadOnly))
    {
        QByteArray response = QString("HTTP/1.1 200 OK\r\n\r\n").toUtf8();
        response += file.readAll();
        socket->write(response);
        file.close();
    }
    else
    {
        throw new CacheFileNotFoundException();
    }
}

void Server::sendOld(QTcpSocket* socket)
{
    QString file = cacheAnalyzator->createOld();
    sendXml(socket, file);
}

void Server::sendNew(QTcpSocket* socket)
{
    QString file = cacheAnalyzator->createNew();
    sendXml(socket, file);
}

void Server::sendPeriod(QTcpSocket* socket, uint from, uint to)
{
    QString file = cacheAnalyzator->createPeriod(from, to);
    sendXml(socket, file);
}

void Server::sendDiskSpace(QTcpSocket* socket)
{
    sendXml(socket, diskAnalyzator->createDiskSpaceInfo());
}

void Server::send404(QTcpSocket* socket)
{
    QByteArray response = QString("HTTP/1.1 404 Not Found\r\n\r\n").toUtf8();
    response += "404 Not Found";
    socket->write(response);
}

void Server::send410(QTcpSocket *socket)
{
    QByteArray response = QString("HTTP/1.1 410 Gone\r\n\r\n").toUtf8();
    response += "410 File Was Delete";
    socket->write(response);
}

void Server::send500(QTcpSocket *socket)
{
    QByteArray response = QString("HTTP/1.1 500 Internal Server Error\r\n\r\n").toUtf8();
    response += "500 Internal Server Error";
    socket->write(response);
}

void Server::sendFile(QTcpSocket* socket, QString path)
{
    QFile* file = new QFile(ROOT_DIRECTORY + path);
    if (file->open(QFile::ReadOnly))
    {
        QByteArray data = QString("HTTP/1.1 200 OK\r\nAccept-Ranges: bytes\r\nContent-Length: ").toUtf8()
                + QString::number(file->size()).toUtf8()
                + QString("\r\nContent-Disposition: inline; filename=\"" + file->fileName() + "\"").toUtf8()
                + QString("\r\nContent-Type: application/x-msdownload\r\n\r\n").toUtf8();
        data += file->readAll();
        socket->write(data);
    }
    else
    {
        throw new FileForDownloadNotFoundException();
    }
}

void Server::sendXml(QTcpSocket *socket, QString data)
{
    QByteArray response = QString("HTTP/1.1 200 OK\r\n\r\n").toUtf8();
    response += data;
    socket->write(response);
}

void Server::onReadyRead()
{
    QTcpSocket* socket = qobject_cast<QTcpSocket*>(sender());
    RequestType* requestType = getRequestType(socket->readAll());

    try
    {
        if (requestType->type == RequestType::CACHE_REQUEST)
        {
            sendCache(socket);
        }
        else if (requestType->type == RequestType::DISK_SPACE_REQUEST)
        {
            sendDiskSpace(socket);
        }
        else if (requestType->type == RequestType::DOWNLOAD_REQUEST)
        {
            sendFile(socket, requestType->getParam("path"));
        }
        else if (requestType->type == RequestType::E404_REQUEST)
        {
            send404(socket);
        }
        else if (requestType->type == RequestType::NEW_REQUEST)
        {
            sendNew(socket);
        }
        else if (requestType->type == RequestType::PERIOD_REQUEST)
        {
            sendPeriod(socket, requestType->getParam("create_date_from").toInt(), requestType->getParam("create_date_to").toInt());
        }
        else if (requestType->type == RequestType::OLD_REQUEST)
        {
            sendOld(socket);
        }
    }
    catch (CacheFileNotFoundException* exception)
    {
        cout << "cache file not found" << endl;
        send410(socket);
    }
    catch (GetDiskInfoException* exception)
    {
        cout << "unexpected error for get disk info operation" << endl;
        send500(socket);
    }
    catch (FileForDownloadNotFoundException* exception)
    {
        cout << "file for download not found" << endl;
        send410(socket);
    }

    cout << "send data" << endl;
    socket->disconnectFromHost();
}

void Server::onDisconnected()
{
    cout << "disconnect" << endl;
    QTcpSocket* socket = qobject_cast<QTcpSocket*>(sender());
    socket->close();
    socket->deleteLater();
}
