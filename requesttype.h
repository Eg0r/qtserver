#ifndef REQUESTTYPE_H
#define REQUESTTYPE_H

#include <QString>
#include <QStringList>
#include <QMap>

#include <QDebug>

class RequestType
{
public:
    RequestType();
    QString getParam(QString key);
    void setParams(QString paramsString);

    int type;
    QMap<QString, QString>* args;

    static const int CACHE_REQUEST;
    static const int OLD_REQUEST;
    static const int NEW_REQUEST;
    static const int DISK_SPACE_REQUEST;
    static const int E404_REQUEST;
    static const int DOWNLOAD_REQUEST;
    static const int PERIOD_REQUEST;
};

#endif // REQUESTTYPE_H
