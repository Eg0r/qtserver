#ifndef DIRECTORYSCANNER_H
#define DIRECTORYSCANNER_H

#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QFileInfoList>
#include <QString>

#include <QDomDocument>
#include <QDomElement>
#include <QTextStream>
#include <QDateTime>
#include <QObject>
#include <QThread>

#include <QDebug>

class DirectoryScanner : public QObject
{

Q_OBJECT

public:
    explicit DirectoryScanner(QObject* parent = 0);
    void walk(QString rootPath);

private:
    void recursiveWalk(QDir* directory, QDomDocument* document, QDomElement* currentDirectory);
    void createInfo(QDomElement* element, QFileInfo* fileInfo);

    QString rootPath;
    QThread* thread;

private slots:
    void threadWalk();

signals:
    void finishedScan();


};

#endif // DIRECTORYSCANNER_H
