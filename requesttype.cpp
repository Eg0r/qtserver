#include "requesttype.h"

const int RequestType::CACHE_REQUEST = 0;
const int RequestType::OLD_REQUEST = 1;
const int RequestType::NEW_REQUEST = 2;
const int RequestType::DISK_SPACE_REQUEST = 3;
const int RequestType::E404_REQUEST = 4;
const int RequestType::DOWNLOAD_REQUEST = 5;
const int RequestType::PERIOD_REQUEST = 6;

RequestType::RequestType()
{
    args = new QMap<QString, QString>();
}

QString RequestType::getParam(QString key)
{
    return args->value(key);
}

void RequestType::setParams(QString paramsString)
{
    QStringList splitingParams = paramsString.split('&');
    for (int i = 0; i < splitingParams.length(); i++)
    {
        QStringList paramsAndValues = splitingParams[i].split('=');
        if (paramsAndValues.length() == 2)
        {
            args->insert(paramsAndValues[0], paramsAndValues[1]);
        }
    }
}
