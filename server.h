#ifndef SERVER_H
#define SERVER_H

#include "requesttype.h"
#include "diskanalyzator.h"
#include "cacheanalyzator.h"

#include "./exception/cachefilenotfoundexception.h"
#include "./exception/getdiskinfoexception.h"
#include "./exception/filefordownloadnotfoundexception.h"

#include <iostream>

#include <QTcpServer>
#include <QTcpSocket>
#include <QString>
#include <QByteArray>
#include <QObject>

#include <QDir>
#include <QFile>
#include <QDataStream>
#include <QFileInfoList>

using namespace  std;

class Server : public QTcpServer
{
    Q_OBJECT

public:
    Server();
    void start();

    const static QString ROOT_DIRECTORY;

private slots:
    void onReadyRead();
    void onDisconnected();
    void incomingConnection(int handle);

private:
    RequestType* getRequestType(QByteArray data);

    CacheAnalyzator* cacheAnalyzator;
    DiskAnalyzator* diskAnalyzator;

    void sendCache(QTcpSocket* socket);
    void sendOld(QTcpSocket* socket);
    void sendNew(QTcpSocket* socket);
    void sendPeriod(QTcpSocket* socket, uint from, uint to);
    void sendDiskSpace(QTcpSocket* socket);   
    void sendFile(QTcpSocket* socket, QString path);
    void sendXml(QTcpSocket *socket, QString data);

    void send404(QTcpSocket* socket);
    void send410(QTcpSocket* socket);
    void send500(QTcpSocket* socket);
};

#endif // SERVER_H
