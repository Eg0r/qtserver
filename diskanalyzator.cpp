#include "diskanalyzator.h"

DiskAnalyzator::DiskAnalyzator()
{

}

QString DiskAnalyzator::createDiskSpaceInfo()
{
    qDebug() << "start analyze avalible space on disk";

    QString sCurDir = QDir::current().absolutePath();

    int f = 1024 * 1024 * 1024;

    ULARGE_INTEGER free,total;
    bool bRes = ::GetDiskFreeSpaceExA( 0 , &free , &total , NULL );
    double fFree;
    double fTotal;
    if (bRes)
    {
        QDir::setCurrent( sCurDir );

        fFree = static_cast<double>( static_cast<__int64>(free.QuadPart) ) / f;
        fTotal = static_cast<double>( static_cast<__int64>(total.QuadPart) ) / f;

        qDebug() << fFree << " " << fTotal;
    }
    else
    {
        throw new GetDiskInfoException();
    }

    QDomDocument* document = new QDomDocument("disk_space");
    QDomProcessingInstruction xmlProcessingInstruction = document->createProcessingInstruction("xml", "version=\"1.0\"");
    document->appendChild(xmlProcessingInstruction);
    QDomElement element = document->createElement("data");
    document->appendChild(element);
    QDomElement freeSpaceElement = document->createElement("data");
    freeSpaceElement.setAttribute("free_space", fFree);
    element.appendChild(freeSpaceElement);
    QDomElement totalSpaceElement = document->createElement("data");
    totalSpaceElement.setAttribute("total_space", fTotal);
    element.appendChild(totalSpaceElement);

    return document->toString();
}
