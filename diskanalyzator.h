#ifndef DISKANALYZER_H
#define DISKANALYZER_H

#include "./exception/getdiskinfoexception.h"

#include <QString>
#include <QDir>
#include <QDomDocument>
#include <QDomProcessingInstruction>
#include <QDomElement>

#include <windows.h>

#include <QDebug>

class DiskAnalyzator
{
public:
    DiskAnalyzator();
    QString createDiskSpaceInfo();
};

#endif // DISKANALYZER_H
