#include <QString>
#include <QtTest>

#include "../directoryscanner.h"
#include "../server.h"

class TestQtServerTest : public QObject
{
    Q_OBJECT

public:
    TestQtServerTest();

private Q_SLOTS:
    void testCase1_DirectoryScanner_Walk();
    //void testCase2_

private slots:
    void testDirectoryScanner_finishedScan();
};

TestQtServerTest::TestQtServerTest()
{
}

void TestQtServerTest::testCase1_DirectoryScanner_Walk()
{   
    DirectoryScanner* directoryScanner = new DirectoryScanner();
    connect(directoryScanner, SIGNAL(finishedScan()), this, SLOT(testDirectoryScanner_finishedScan()));
    directoryScanner->walk(Server::ROOT_DIRECTORY);
}

void TestQtServerTest::testDirectoryScanner_finishedScan()
{
    qDebug() << "test slot scan finished";
    QVERIFY2(true, "Failure");
}

QTEST_APPLESS_MAIN(TestQtServerTest)

#include "tst_testqtservertest.moc"
