#ifndef CACHEFILENOTFOUNDEXCEPTION_H
#define CACHEFILENOTFOUNDEXCEPTION_H

#include <QException>

class CacheFileNotFoundException : public QException
{
public:
};

#endif // CACHEFILENOTFOUNDEXCEPTION_H
