#ifndef FILEFORDOWNLOADNOTFOUNDEXCEPTION_H
#define FILEFORDOWNLOADNOTFOUNDEXCEPTION_H

#include <QException>

class FileForDownloadNotFoundException : public QException
{
public:
};

#endif // FILEFORDOWNLOADNOTFOUNDEXCEPTION_H
