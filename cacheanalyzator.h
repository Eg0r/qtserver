#ifndef CACHEANALYZATOR_H
#define CACHEANALYZATOR_H

#include "./exception/cachefilenotfoundexception.h"

#include <QDateTime>
#include <QXmlQuery>
#include <QDomDocument>
#include <QDomElement>
#include <QDomProcessingInstruction>
#include <QDomNodeList>
#include <QDomNode>
#include <QFile>

#include <QDebug>

class CacheAnalyzator
{
public:
    CacheAnalyzator();
    QString createPeriod(uint dateTime1, uint dateTime2);
    QString createOld();
    QString createNew();

private:
    QDomNodeList xPathQueryRun(QString query);
};

#endif // CACHEANALYZATOR_H
